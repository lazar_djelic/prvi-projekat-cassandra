﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Podaci
{
    public class Narucilac : Object
    {
        Int32 _brLicneKarte;
        String _ime;
        String _prezime;
        String _adresa;
        String _brojTelefona;
        String _email;

        public Int32 Licna_Karta
        {
            get { return _brLicneKarte; }
            set { _brLicneKarte = value; }
        }

        public String Ime
        {
            get { return _ime; }
            set { _ime = value; }
        }

        public String Prezime
        {
            get { return _prezime; }
            set { _prezime = value; }
        }

        public String Adresa
        {
            get { return _adresa; }
            set { _adresa = value; }
        }

        public String Broj_Telefona
        {
            get { return _brojTelefona; }
            set { _brojTelefona = value; }
        }

        public String Email
        {
            get { return _email; }
            set { _email = value; }
        }

        public Narucilac()
        {
        }

        public Narucilac(Int32 brLicneKarte, String ime, String prezime, String adresa, String brojTelefona, String email)
        {
            _brLicneKarte = brLicneKarte;
            _ime = ime;
            _prezime = prezime;
            _adresa = adresa;
            _brojTelefona = brojTelefona;
            _email = email;
        }
    }
}
