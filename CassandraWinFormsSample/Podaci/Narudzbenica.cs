﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace Podaci
{
    public class Narudzbenica : Object
    {
        #region Atributi
        Int64 _orderID;
        String _purchasedOn;
        Int32 _narucilac;
        Decimal _income;
        String _status;
        String _dateRequired;
        String _dateShipped;
        String _shipVia;
        Decimal _freightCharges;
        #endregion

        #region Interfejsi
        public Int64 Order_ID
        {
            get { return _orderID; }
            set { _orderID = value; }
        }

        public String Purchased_On
        {
            get { return _purchasedOn; }
            set { _purchasedOn = value; }
        }

        public Int32 Narucilac
        {
            get { return _narucilac; }
            set { _narucilac = value; }
        }

        public Decimal Income
        {
            get { return _income; }
            set { _income = value; }
        }

        public String Status
        {
            get { return _status; }
            set { _status = value; }
        }

        public String Date_Required
        {
            get { return _dateRequired; }
            set { _dateRequired = value; }
        }

        public String Date_Shipped
        {
            get { return _dateShipped; }
            set { _dateShipped = value; }
        }

        public String Ship_Via
        {
            get { return _shipVia; }
            set { _shipVia = value; }
        }

        public Decimal Freight_Charges
        {
            get { return _freightCharges; }
            set { _freightCharges = value; }
        }
        #endregion

        #region Metodi
        public Narudzbenica()
        {

        }

        public Narudzbenica(Int64 orderID, String purchasedOn, String billTo, String shipTo, Decimal income, String status, String dateRequired, String dateShipped, String shipVia, Decimal freightCharges, Int32 narucilac)
        {
            _orderID = orderID;
            _purchasedOn = purchasedOn;
            _income = income;
            _status = status;
            _dateRequired = dateRequired;
            _dateShipped = dateShipped;
            _shipVia = shipVia;
            _freightCharges = freightCharges;
            _narucilac = narucilac;
        }
        #endregion
    }
}
