﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Podaci
{
    public class Stavka : Object
    {
        Int64 _stavkaID;
        String _naziv;
        Decimal _cena;
        Int32 _kolicina;
        Int64 _idNarudzbenice;

        public Int64 Item_ID
        {
            get { return _stavkaID; }
            set { _stavkaID = value; }
        }

        public Int64 OrderID
        {
            get { return _idNarudzbenice; }
            set { _idNarudzbenice = value; }
        }

        public String Product_Name
        {
            get { return _naziv; }
            set { _naziv = value; }
        }

        public Decimal Unit_Price
        {
            get { return _cena; }
            set { _cena = value; }
        }

        public Int32 Quantity
        {
            get { return _kolicina; }
            set { _kolicina = value; }
        }

        public Stavka()
        {
        }

        public Stavka(String naziv, Decimal cena, Int32 kolicina)
        {
            _naziv = naziv;
            _cena = cena;
            _kolicina = kolicina;
        }
    }
}
