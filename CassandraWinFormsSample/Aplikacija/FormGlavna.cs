﻿using Podaci;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using MetroFramework.Forms;

namespace Aplikacija
{
    public partial class FormGlavna : MetroForm
    {
        private bool _formaZatvorena;
        private List<Narudzbenica> ListaNarudzbenica;
        public Narudzbenica Nar
        {
            get;
            set;
        }

        public FormGlavna()
        {
            ListaNarudzbenica = new List<Narudzbenica>();

            InitializeComponent();
        }

        DialogResult ZatvoriFormu()
        {
            return MessageBox.Show("Da li ste sigurni da izvrsite izabranu akciju?", "Obavestenje", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
        }

        public String NadjiPrviDatum()
        {
            DateTime prvi = DateTime.Today;
            foreach (var n in ListaNarudzbenica)
            {
                if (DateTime.Parse(n.Purchased_On) < prvi)
                    prvi = DateTime.Parse(n.Purchased_On);
            }
            return prvi.ToString();
        }

        List<Narudzbenica> SortList(List<Narudzbenica> lista)
        {
            for (int i = 0; i < lista.Count - 1; i++)
            {
                Int64 id = lista[i].Order_ID;
                for (int j = i + 1; j < lista.Count; j++)
                {
                    Int64 id2 = lista[j].Order_ID;
                    if (id > id2)
                    {
                        Narudzbenica tmp = lista[i];
                        lista[i] = lista[j];
                        lista[j] = tmp;
                    }
                }
            }
            return lista;
        }

        void UcitajPodatke(List<Narudzbenica> izvor)
        {
            if (izvor == ListaNarudzbenica)
            {
                ListaNarudzbenica = RadSaBazom.ucitajNarudzbeniceIzBaze();
                izvor = ListaNarudzbenica;
            }

            dgvPodaci.DataSource = SortList(izvor).ToList();
            dgvPodaci.Columns["Date_Required"].Visible = false;
            dgvPodaci.Columns["Date_Shipped"].Visible = false;
            dgvPodaci.Columns["Ship_Via"].Visible = false;
            dgvPodaci.Columns["Freight_Charges"].Visible = false;

            if (dgvPodaci.RowCount > 0)
            {
                txtID.Enabled = true;
                txtOrdererID.Enabled = true;
                dtpOd.Enabled = true;
                dtpDo.Enabled = true;
                cmbStatus.Enabled = true;
                btnEdit.Enabled = true;
                btnDelete.Enabled = true;
                btnFilter.Enabled = true;
                btnClear.Enabled = true;
                btnPending.Enabled = true;
                btnProcessing.Enabled = true;
                btnComplete.Enabled = true;
                dtpOd.Value = System.DateTime.Parse(NadjiPrviDatum());
            }
            else
            {
                txtID.Enabled = false;
                txtOrdererID.Enabled = false;
                dtpOd.Enabled = false;
                dtpDo.Enabled = false;
                cmbStatus.Enabled = false;
                btnEdit.Enabled = false;
                btnDelete.Enabled = false;
                btnFilter.Enabled = false;
                btnClear.Enabled = false;
                btnPending.Enabled = false;
                btnProcessing.Enabled = false;
                btnComplete.Enabled = false;
            }
        }

        private void FormGlavna_Load(object sender, EventArgs e)
        {
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            UcitajPodatke(ListaNarudzbenica);
            dgvPodaci.Columns[0].HeaderText = "Order #";
            dgvPodaci.Columns[1].HeaderText = "Purchased On";
            dgvPodaci.Columns[2].HeaderText = "Orderer ID";
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            var forma = new FormNova(new Narudzbenica());
            DialogResult dr = forma.ShowDialog();

            if (dr == System.Windows.Forms.DialogResult.OK)
                UcitajPodatke(ListaNarudzbenica);
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            if (dgvPodaci.SelectedRows.Count == 0)
                return;

            int izabranaNarudzbenica = dgvPodaci.SelectedRows[0].Index;
            Int64 id = (Int64)dgvPodaci["Order_ID", izabranaNarudzbenica].Value;

            Narudzbenica narudzbenica = RadSaBazom.vratiNarudzbenicu(id);

            var frm = new FormNova(narudzbenica);

            DialogResult dlg = frm.ShowDialog();

            if (dlg == System.Windows.Forms.DialogResult.OK)
                UcitajPodatke(ListaNarudzbenica);
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            if (dgvPodaci.SelectedRows.Count == 0)
                return;

            DialogResult dlg = MessageBox.Show("Da li ste sigurni da zelite da obrisete izabranu stavku?", "Obavestenje", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

            if (dlg == System.Windows.Forms.DialogResult.No)
                return;

            int izabranaNarudzbenica = dgvPodaci.SelectedRows[0].Index;
            Int64 id = (Int64)dgvPodaci["Order_ID", izabranaNarudzbenica].Value;

            RadSaBazom.obrisiNarudzbenicu(id);

            UcitajPodatke(ListaNarudzbenica);
        }

        private void btnFilter_Click(object sender, EventArgs e)
        {
            Int64 id;
            Int64 narucilcID;
            List<Narudzbenica> lista = new List<Narudzbenica>();
            foreach (Narudzbenica n in ListaNarudzbenica)
            {
                if (txtID.Text == "" && txtOrdererID.Text == "" && dtpOd.Value <= DateTime.Parse(n.Purchased_On) && dtpDo.Value >= DateTime.Parse(n.Purchased_On) && cmbStatus.Text == n.Status && dtpOd.Value <= dtpDo.Value)
                {
                    lista.Add(n); //bez id-jeva
                    continue;
                }

                if (txtID.Text == "" && txtOrdererID.Text == "" && dtpOd.Value <= DateTime.Parse(n.Purchased_On) && dtpDo.Value >= DateTime.Parse(n.Purchased_On) && cmbStatus.Text == "" && dtpOd.Value <= dtpDo.Value)
                {
                    lista.Add(n); //bez id-jeva i statusa
                    continue;
                }

                if (Int64.TryParse(txtID.Text, out id))
                {
                    if (Int64.TryParse(txtOrdererID.Text, out narucilcID))
                    {
                        if (Int64.Parse(txtID.Text) == n.Order_ID && Int64.Parse(txtOrdererID.Text) == n.Narucilac && dtpOd.Value <= DateTime.Parse(n.Purchased_On) && dtpDo.Value >= DateTime.Parse(n.Purchased_On) && cmbStatus.Text == n.Status && dtpOd.Value <= dtpDo.Value)
                        {
                            lista.Add(n); //sa svim unetim kriterijumima
                            continue;
                        }

                        if (Int64.Parse(txtID.Text) == n.Order_ID && Int64.Parse(txtOrdererID.Text) == n.Narucilac && dtpOd.Value == DateTime.Parse(NadjiPrviDatum()) && dtpDo.Value.Date == DateTime.Today.Date && cmbStatus.Text == "")
                        {
                            lista.Add(n); //bez statusa i datuma
                            continue;
                        }

                        if (Int64.Parse(txtID.Text) == n.Order_ID && Int64.Parse(txtOrdererID.Text) == n.Narucilac && dtpOd.Value == DateTime.Parse(NadjiPrviDatum()) && dtpDo.Value.Date == DateTime.Today.Date && cmbStatus.Text == n.Status)
                        {
                            lista.Add(n); //bez datuma
                            continue;
                        }

                        if (Int64.Parse(txtID.Text) == n.Order_ID && Int64.Parse(txtOrdererID.Text) == n.Narucilac && dtpOd.Value <= DateTime.Parse(n.Purchased_On) && dtpDo.Value >= DateTime.Parse(n.Purchased_On) && cmbStatus.Text == "" && dtpOd.Value <= dtpDo.Value)
                        {
                            lista.Add(n); //bez statusa
                            continue;
                        }
                    }

                    if (Int64.Parse(txtID.Text) == n.Order_ID && txtOrdererID.Text == "" && dtpOd.Value <= DateTime.Parse(n.Purchased_On) && dtpDo.Value >= DateTime.Parse(n.Purchased_On) && cmbStatus.Text == n.Status && dtpOd.Value <= dtpDo.Value)
                    {
                        lista.Add(n); //bez ordererID-a
                        continue;
                    }

                    if (Int64.Parse(txtID.Text) == n.Order_ID && txtOrdererID.Text == "" && dtpOd.Value == DateTime.Parse(NadjiPrviDatum()) && dtpDo.Value.Date == DateTime.Today.Date && cmbStatus.Text == "")
                    {
                        lista.Add(n); //bez statusa, datuma i ordererID-a
                        continue;
                    }

                    if (Int64.Parse(txtID.Text) == n.Order_ID && txtOrdererID.Text == "" && dtpOd.Value == DateTime.Parse(NadjiPrviDatum()) && dtpDo.Value.Date == DateTime.Today.Date && cmbStatus.Text == n.Status)
                    {
                        lista.Add(n); //bez datuma i ordererID-a
                        continue;
                    }

                    if (Int64.Parse(txtID.Text) == n.Order_ID && txtOrdererID.Text == "" && dtpOd.Value <= DateTime.Parse(n.Purchased_On) && dtpDo.Value >= DateTime.Parse(n.Purchased_On) && cmbStatus.Text == "" && dtpOd.Value <= dtpDo.Value)
                    {
                        lista.Add(n); //bez statusa i ordererID-a
                        continue;
                    }
                }

                if (Int64.TryParse(txtOrdererID.Text, out narucilcID))
                {
                    if (Int64.Parse(txtOrdererID.Text) == n.Narucilac && txtID.Text == "" && dtpOd.Value <= DateTime.Parse(n.Purchased_On) && dtpDo.Value >= DateTime.Parse(n.Purchased_On) && cmbStatus.Text == n.Status && dtpOd.Value <= dtpDo.Value)
                    {
                        lista.Add(n); //bez ID-a
                        continue;
                    }

                    if (Int64.Parse(txtOrdererID.Text) == n.Narucilac && txtID.Text == "" && dtpOd.Value == DateTime.Parse(NadjiPrviDatum()) && dtpDo.Value.Date == DateTime.Today.Date && cmbStatus.Text == "")
                    {
                        lista.Add(n); //bez statusa, datuma i ID-a
                        continue;
                    }

                    if (Int64.Parse(txtOrdererID.Text) == n.Narucilac && txtID.Text == "" && dtpOd.Value == DateTime.Parse(NadjiPrviDatum()) && dtpDo.Value.Date == DateTime.Today.Date && cmbStatus.Text == n.Status)
                    {
                        lista.Add(n); //bez datuma i ordererID-a
                        continue;
                    }

                    if (Int64.Parse(txtOrdererID.Text) == n.Narucilac && txtID.Text == "" && dtpOd.Value <= DateTime.Parse(n.Purchased_On) && dtpDo.Value >= DateTime.Parse(n.Purchased_On) && cmbStatus.Text == "" && dtpOd.Value <= dtpDo.Value)
                    {
                        lista.Add(n); //bez statusa i ordererID-a
                        continue;
                    }
                }
            }
            if (lista.Count != 0)
            {
                dgvPodaci.DataSource = lista.ToList();
                UcitajPodatke(lista);
            }
            else
            {
                MessageBox.Show("Ne postoji narudzbenica u listi sa unetim kriterijumima");
                return;
            }

            if (txtID.Text.Length < 8 && txtID.Text.Length > 0)
            {
                MessageBox.Show("Order ID mora sadrzati 8 cifara.", "Obavestenje", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void btnClear_Click(object sender, EventArgs e)
        {
            txtID.Text = "";
            cmbStatus.Text = "";
            txtOrdererID.Text = "";
            dtpOd.Value = DateTime.Parse(NadjiPrviDatum());
            dtpDo.Value = DateTime.Today;
            UcitajPodatke(ListaNarudzbenica);
        }

        public void promeniStatusNarudzbenici(String statusN)
        {
            if (dgvPodaci.SelectedRows.Count == 0)
                return;

            int izabranaNarudzbenica = dgvPodaci.SelectedRows[0].Index;
            Int64 id = (Int64)dgvPodaci["Order_ID", izabranaNarudzbenica].Value;

            RadSaBazom.promeniStatusNarudzbenici(statusN, id);

            UcitajPodatke(ListaNarudzbenica);
        }

        private void btnPending_Click(object sender, EventArgs e)
        {
            promeniStatusNarudzbenici("Pending");
        }

        private void btnProcessing_Click(object sender, EventArgs e)
        {
            promeniStatusNarudzbenici("Processing");
        }

        private void btnComplete_Click(object sender, EventArgs e)
        {
            promeniStatusNarudzbenici("Complete");
        }

        private void txtID_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!Char.IsDigit(e.KeyChar) && !Char.IsControl(e.KeyChar))
                e.Handled = true;
        }

        private void metroButton1_Click(object sender, EventArgs e)
        {

        }

        private void metroTile1_Click(object sender, EventArgs e)
        {

        }

        private void metroButton1_Click_1(object sender, EventArgs e)
        {

        }

        private void metroButton1_Click_2(object sender, EventArgs e)
        {

        }


        private void btnAdd_Click_1(object sender, EventArgs e)
        {

        }

        private void btnEdit_Click_1(object sender, EventArgs e)
        {

        }

        private void btnDelete_Click_1(object sender, EventArgs e)
        {

        }
    }
}
