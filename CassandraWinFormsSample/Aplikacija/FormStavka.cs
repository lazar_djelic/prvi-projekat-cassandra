﻿using Podaci;
using Ekstenzije;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MetroSet_UI.Forms;
using MetroFramework.Forms;
namespace Aplikacija
{
    public partial class FormStavka : MetroForm
    {
        bool tacka = false;
        private Narudzbenica nar;
        private Int64 id;

        public Stavka Item
        {
            get;
            set;
        }

        public FormStavka(Narudzbenica n, Int32 idStavke)
        {
            nar = n;
            id = ++idStavke;
            InitializeComponent();
        }

        public FormStavka(Stavka s, Narudzbenica n)
        {
            nar = n;
            InitializeComponent();
            Item = s;
            btnDodaj.Text = "Edit";
        }

        bool Provera()
        {
            if (String.IsNullOrEmpty(txtProductName.Text))
            {
                MessageBox.Show("Morate uneti naziv proizvoda!", "Obavestenje", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }

            if (String.IsNullOrEmpty(txtUnitPrice.Text))
            {
                MessageBox.Show("Morate uneti cenu proizvoda!", "Obavestenje", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }

            if (String.IsNullOrEmpty(txtQuantity.Text))
            {
                MessageBox.Show("Morate uneti kolicinu!", "Obavestenje", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }

            return true;
        }

        void UcitajPodatkeUKontrole()
        {
            txtProductName.Text = Item.Product_Name;
            txtUnitPrice.Text = Item.Unit_Price.ToString();
            //txtUnitPrice.Text = Item.Unit_Price.ToString("0.00");
            txtQuantity.Text = Item.Quantity.ToString();

            btnDodaj.Text = "Edit";
        }

        void Dodaj()
        {
            Decimal c;
            Decimal.TryParse(txtUnitPrice.Text, out c);
            Int32 k;
            Int32.TryParse(txtQuantity.Text, out k);

            RadSaBazom.dodajStavku(id, txtProductName.Text, c, k, nar.Order_ID);

            return;
        }

        void Izmeni()
        {
            Decimal c;
            Decimal.TryParse(txtUnitPrice.Text, out c);
            Int32 k;
            Int32.TryParse(txtQuantity.Text, out k);

            RadSaBazom.izmeniStavku(Item.Item_ID, txtProductName.Text, c, k, nar.Order_ID);

            return;
        }

        private void FormStavka_Load(object sender, EventArgs e)
        {
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            if (Item != null)
                UcitajPodatkeUKontrole();
        }

        private void btnDodaj_Click(object sender, EventArgs e)
        {
            if (!Provera())
                return;

            if (Item == null)
                Dodaj();
            else
                Izmeni();

            this.Close();
            DialogResult = System.Windows.Forms.DialogResult.OK;
        }

        private void txtProductName_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!Char.IsLetter(e.KeyChar) && !Char.IsDigit(e.KeyChar) && !Char.IsControl(e.KeyChar) && e.KeyChar.ToString() != " ")
                e.Handled = true;
        }

        private void txtProductName_Leave(object sender, EventArgs e)
        {
            txtProductName.Text = txtProductName.Text.PostaviPrvoVelikoSlovo();
        }

        private void txtUnitPrice_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!tacka)
            {
                if (!Char.IsDigit(e.KeyChar) && !Char.IsControl(e.KeyChar) && e.KeyChar.ToString() != ".")
                    e.Handled = true;
            }
            else
                if (!Char.IsDigit(e.KeyChar) && !Char.IsControl(e.KeyChar))
                    e.Handled = true;

            if (e.KeyChar.ToString() == ".") { tacka = true; }
        }

        private void txtQuantity_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!Char.IsDigit(e.KeyChar) && !Char.IsControl(e.KeyChar))
                e.Handled = true;
        }

        private void btnDodaj_Click_1(object sender, EventArgs e)
        {

        }
    }
}
