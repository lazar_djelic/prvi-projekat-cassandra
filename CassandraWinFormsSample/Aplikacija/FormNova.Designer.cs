﻿namespace Aplikacija
{
    partial class FormNova
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            this.lblOrderID = new MetroFramework.Controls.MetroTile();
            this.lblOrderDate = new MetroFramework.Controls.MetroTile();
            this.txtOrderID = new MetroFramework.Controls.MetroTextBox();
            this.dtpOrderDate = new MetroFramework.Controls.MetroDateTime();
            this.lblDateRequired = new MetroFramework.Controls.MetroTile();
            this.lblDateShipped = new MetroFramework.Controls.MetroTile();
            this.lblShipVia = new MetroFramework.Controls.MetroTile();
            this.lblFreightCharges = new MetroFramework.Controls.MetroTile();
            this.dtpDateRequired = new MetroFramework.Controls.MetroDateTime();
            this.dtpDateShipped = new MetroFramework.Controls.MetroDateTime();
            this.txtShipVia = new MetroFramework.Controls.MetroTextBox();
            this.txtFreightCharges = new MetroFramework.Controls.MetroTextBox();
            this.lblCustomer = new MetroFramework.Controls.MetroTile();
            this.label1 = new MetroFramework.Controls.MetroLabel();
            this.label2 = new MetroFramework.Controls.MetroLabel();
            this.label3 = new MetroFramework.Controls.MetroLabel();
            this.label4 = new MetroFramework.Controls.MetroLabel();
            this.label5 = new MetroFramework.Controls.MetroLabel();
            this.label6 = new MetroFramework.Controls.MetroLabel();
            this.lblOrderList = new MetroFramework.Controls.MetroLabel();
            this.lblStatus = new MetroFramework.Controls.MetroLabel();
            this.btnDodajStavku = new MetroFramework.Controls.MetroButton();
            this.btnIzmeniStavku = new MetroFramework.Controls.MetroButton();
            this.btnObrisiStavku = new MetroFramework.Controls.MetroButton();
            this.btnOK = new MetroFramework.Controls.MetroButton();
            this.btnCancel = new MetroFramework.Controls.MetroButton();
            this.txtBrLicKar = new MetroFramework.Controls.MetroTextBox();
            this.txtIme = new MetroFramework.Controls.MetroTextBox();
            this.txtPrezime = new MetroFramework.Controls.MetroTextBox();
            this.txtAdresa = new MetroFramework.Controls.MetroTextBox();
            this.txtBrTel = new MetroFramework.Controls.MetroTextBox();
            this.txtEmail = new MetroFramework.Controls.MetroTextBox();
            this.cmbStatus = new MetroFramework.Controls.MetroComboBox();
            this.dgvOrderList = new MetroFramework.Controls.MetroGrid();
            ((System.ComponentModel.ISupportInitialize)(this.dgvOrderList)).BeginInit();
            this.SuspendLayout();
            // 
            // lblOrderID
            // 
            this.lblOrderID.ActiveControl = null;
            this.lblOrderID.Location = new System.Drawing.Point(256, 34);
            this.lblOrderID.Name = "lblOrderID";
            this.lblOrderID.Size = new System.Drawing.Size(98, 37);
            this.lblOrderID.TabIndex = 19;
            this.lblOrderID.Text = "Order ID";
            this.lblOrderID.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lblOrderID.UseSelectable = true;
            // 
            // lblOrderDate
            // 
            this.lblOrderDate.ActiveControl = null;
            this.lblOrderDate.Location = new System.Drawing.Point(357, 34);
            this.lblOrderDate.Name = "lblOrderDate";
            this.lblOrderDate.Size = new System.Drawing.Size(98, 37);
            this.lblOrderDate.TabIndex = 20;
            this.lblOrderDate.Text = "Order Date";
            this.lblOrderDate.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lblOrderDate.UseSelectable = true;
            // 
            // txtOrderID
            // 
            // 
            // 
            // 
            this.txtOrderID.CustomButton.Image = null;
            this.txtOrderID.CustomButton.Location = new System.Drawing.Point(70, 1);
            this.txtOrderID.CustomButton.Name = "";
            this.txtOrderID.CustomButton.Size = new System.Drawing.Size(27, 27);
            this.txtOrderID.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.txtOrderID.CustomButton.TabIndex = 1;
            this.txtOrderID.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.txtOrderID.CustomButton.UseSelectable = true;
            this.txtOrderID.CustomButton.Visible = false;
            this.txtOrderID.Lines = new string[0];
            this.txtOrderID.Location = new System.Drawing.Point(256, 71);
            this.txtOrderID.MaxLength = 8;
            this.txtOrderID.Name = "txtOrderID";
            this.txtOrderID.PasswordChar = '\0';
            this.txtOrderID.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtOrderID.SelectedText = "";
            this.txtOrderID.SelectionLength = 0;
            this.txtOrderID.SelectionStart = 0;
            this.txtOrderID.ShortcutsEnabled = true;
            this.txtOrderID.Size = new System.Drawing.Size(98, 29);
            this.txtOrderID.TabIndex = 0;
            this.txtOrderID.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.txtOrderID.UseSelectable = true;
            this.txtOrderID.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.txtOrderID.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            this.txtOrderID.Click += new System.EventHandler(this.txtOrderID_Click);
            this.txtOrderID.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtOrderID_KeyPress);
            // 
            // dtpOrderDate
            // 
            this.dtpOrderDate.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpOrderDate.Location = new System.Drawing.Point(357, 71);
            this.dtpOrderDate.MinimumSize = new System.Drawing.Size(0, 29);
            this.dtpOrderDate.Name = "dtpOrderDate";
            this.dtpOrderDate.Size = new System.Drawing.Size(98, 29);
            this.dtpOrderDate.TabIndex = 1;
            this.dtpOrderDate.Theme = MetroFramework.MetroThemeStyle.Dark;
            // 
            // lblDateRequired
            // 
            this.lblDateRequired.ActiveControl = null;
            this.lblDateRequired.Location = new System.Drawing.Point(23, 114);
            this.lblDateRequired.Name = "lblDateRequired";
            this.lblDateRequired.Size = new System.Drawing.Size(122, 38);
            this.lblDateRequired.TabIndex = 21;
            this.lblDateRequired.Text = "Date Required";
            this.lblDateRequired.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lblDateRequired.UseSelectable = true;
            // 
            // lblDateShipped
            // 
            this.lblDateShipped.ActiveControl = null;
            this.lblDateShipped.Location = new System.Drawing.Point(151, 114);
            this.lblDateShipped.Name = "lblDateShipped";
            this.lblDateShipped.Size = new System.Drawing.Size(122, 38);
            this.lblDateShipped.TabIndex = 22;
            this.lblDateShipped.Text = "Date Shipped";
            this.lblDateShipped.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lblDateShipped.UseSelectable = true;
            // 
            // lblShipVia
            // 
            this.lblShipVia.ActiveControl = null;
            this.lblShipVia.Location = new System.Drawing.Point(279, 114);
            this.lblShipVia.Name = "lblShipVia";
            this.lblShipVia.Size = new System.Drawing.Size(122, 38);
            this.lblShipVia.TabIndex = 23;
            this.lblShipVia.Text = "Ship Via";
            this.lblShipVia.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lblShipVia.UseSelectable = true;
            // 
            // lblFreightCharges
            // 
            this.lblFreightCharges.ActiveControl = null;
            this.lblFreightCharges.Location = new System.Drawing.Point(407, 114);
            this.lblFreightCharges.Name = "lblFreightCharges";
            this.lblFreightCharges.Size = new System.Drawing.Size(122, 38);
            this.lblFreightCharges.TabIndex = 24;
            this.lblFreightCharges.Text = "Freight Charges";
            this.lblFreightCharges.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lblFreightCharges.UseSelectable = true;
            // 
            // dtpDateRequired
            // 
            this.dtpDateRequired.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpDateRequired.Location = new System.Drawing.Point(23, 151);
            this.dtpDateRequired.MinimumSize = new System.Drawing.Size(0, 29);
            this.dtpDateRequired.Name = "dtpDateRequired";
            this.dtpDateRequired.Size = new System.Drawing.Size(122, 29);
            this.dtpDateRequired.TabIndex = 2;
            this.dtpDateRequired.Theme = MetroFramework.MetroThemeStyle.Dark;
            // 
            // dtpDateShipped
            // 
            this.dtpDateShipped.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpDateShipped.Location = new System.Drawing.Point(151, 151);
            this.dtpDateShipped.MinimumSize = new System.Drawing.Size(0, 29);
            this.dtpDateShipped.Name = "dtpDateShipped";
            this.dtpDateShipped.Size = new System.Drawing.Size(122, 29);
            this.dtpDateShipped.TabIndex = 3;
            this.dtpDateShipped.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.dtpDateShipped.ValueChanged += new System.EventHandler(this.dtpDateShipped_ValueChanged);
            // 
            // txtShipVia
            // 
            // 
            // 
            // 
            this.txtShipVia.CustomButton.Image = null;
            this.txtShipVia.CustomButton.Location = new System.Drawing.Point(94, 1);
            this.txtShipVia.CustomButton.Name = "";
            this.txtShipVia.CustomButton.Size = new System.Drawing.Size(27, 27);
            this.txtShipVia.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.txtShipVia.CustomButton.TabIndex = 1;
            this.txtShipVia.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.txtShipVia.CustomButton.UseSelectable = true;
            this.txtShipVia.CustomButton.Visible = false;
            this.txtShipVia.Lines = new string[0];
            this.txtShipVia.Location = new System.Drawing.Point(279, 151);
            this.txtShipVia.MaxLength = 32767;
            this.txtShipVia.Name = "txtShipVia";
            this.txtShipVia.PasswordChar = '\0';
            this.txtShipVia.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtShipVia.SelectedText = "";
            this.txtShipVia.SelectionLength = 0;
            this.txtShipVia.SelectionStart = 0;
            this.txtShipVia.ShortcutsEnabled = true;
            this.txtShipVia.Size = new System.Drawing.Size(122, 29);
            this.txtShipVia.TabIndex = 4;
            this.txtShipVia.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.txtShipVia.UseSelectable = true;
            this.txtShipVia.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.txtShipVia.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            this.txtShipVia.Click += new System.EventHandler(this.txtShipVia_Click);
            this.txtShipVia.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtShipVia_KeyPress);
            this.txtShipVia.Leave += new System.EventHandler(this.txtShipVia_Leave);
            // 
            // txtFreightCharges
            // 
            // 
            // 
            // 
            this.txtFreightCharges.CustomButton.Image = null;
            this.txtFreightCharges.CustomButton.Location = new System.Drawing.Point(94, 1);
            this.txtFreightCharges.CustomButton.Name = "";
            this.txtFreightCharges.CustomButton.Size = new System.Drawing.Size(27, 27);
            this.txtFreightCharges.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.txtFreightCharges.CustomButton.TabIndex = 1;
            this.txtFreightCharges.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.txtFreightCharges.CustomButton.UseSelectable = true;
            this.txtFreightCharges.CustomButton.Visible = false;
            this.txtFreightCharges.Lines = new string[0];
            this.txtFreightCharges.Location = new System.Drawing.Point(407, 151);
            this.txtFreightCharges.MaxLength = 32767;
            this.txtFreightCharges.Name = "txtFreightCharges";
            this.txtFreightCharges.PasswordChar = '\0';
            this.txtFreightCharges.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtFreightCharges.SelectedText = "";
            this.txtFreightCharges.SelectionLength = 0;
            this.txtFreightCharges.SelectionStart = 0;
            this.txtFreightCharges.ShortcutsEnabled = true;
            this.txtFreightCharges.Size = new System.Drawing.Size(122, 29);
            this.txtFreightCharges.TabIndex = 5;
            this.txtFreightCharges.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.txtFreightCharges.UseSelectable = true;
            this.txtFreightCharges.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.txtFreightCharges.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            this.txtFreightCharges.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtFreightCharges_KeyPress);
            // 
            // lblCustomer
            // 
            this.lblCustomer.ActiveControl = null;
            this.lblCustomer.Location = new System.Drawing.Point(23, 201);
            this.lblCustomer.Name = "lblCustomer";
            this.lblCustomer.Size = new System.Drawing.Size(506, 38);
            this.lblCustomer.TabIndex = 33;
            this.lblCustomer.Text = "Customer";
            this.lblCustomer.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lblCustomer.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.lblCustomer.UseSelectable = true;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(67, 261);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(54, 19);
            this.label1.TabIndex = 25;
            this.label1.Text = "ID num:";
            this.label1.Theme = MetroFramework.MetroThemeStyle.Dark;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(48, 289);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(73, 19);
            this.label2.TabIndex = 26;
            this.label2.Text = "First name:";
            this.label2.Theme = MetroFramework.MetroThemeStyle.Dark;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(50, 314);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(71, 19);
            this.label3.TabIndex = 27;
            this.label3.Text = "Last name:";
            this.label3.Theme = MetroFramework.MetroThemeStyle.Dark;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(292, 261);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(59, 19);
            this.label4.TabIndex = 28;
            this.label4.Text = "Address:";
            this.label4.Theme = MetroFramework.MetroThemeStyle.Dark;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(302, 289);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(49, 19);
            this.label5.TabIndex = 29;
            this.label5.Text = "Phone:";
            this.label5.Theme = MetroFramework.MetroThemeStyle.Dark;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(307, 314);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(44, 19);
            this.label6.TabIndex = 30;
            this.label6.Text = "Email:";
            this.label6.Theme = MetroFramework.MetroThemeStyle.Dark;
            // 
            // lblOrderList
            // 
            this.lblOrderList.AutoSize = true;
            this.lblOrderList.Location = new System.Drawing.Point(238, 353);
            this.lblOrderList.Name = "lblOrderList";
            this.lblOrderList.Size = new System.Drawing.Size(67, 19);
            this.lblOrderList.TabIndex = 31;
            this.lblOrderList.Text = "Order List";
            this.lblOrderList.Theme = MetroFramework.MetroThemeStyle.Dark;
            // 
            // lblStatus
            // 
            this.lblStatus.AutoSize = true;
            this.lblStatus.Location = new System.Drawing.Point(28, 636);
            this.lblStatus.Name = "lblStatus";
            this.lblStatus.Size = new System.Drawing.Size(46, 19);
            this.lblStatus.TabIndex = 32;
            this.lblStatus.Text = "Status:";
            this.lblStatus.Theme = MetroFramework.MetroThemeStyle.Dark;
            // 
            // btnDodajStavku
            // 
            this.btnDodajStavku.Location = new System.Drawing.Point(28, 582);
            this.btnDodajStavku.Name = "btnDodajStavku";
            this.btnDodajStavku.Size = new System.Drawing.Size(105, 35);
            this.btnDodajStavku.TabIndex = 13;
            this.btnDodajStavku.Text = "Add item";
            this.btnDodajStavku.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.btnDodajStavku.UseSelectable = true;
            this.btnDodajStavku.Click += new System.EventHandler(this.btnDodajStavku_Click);
            // 
            // btnIzmeniStavku
            // 
            this.btnIzmeniStavku.Location = new System.Drawing.Point(142, 582);
            this.btnIzmeniStavku.Name = "btnIzmeniStavku";
            this.btnIzmeniStavku.Size = new System.Drawing.Size(105, 35);
            this.btnIzmeniStavku.TabIndex = 14;
            this.btnIzmeniStavku.Text = "Edit item";
            this.btnIzmeniStavku.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.btnIzmeniStavku.UseSelectable = true;
            this.btnIzmeniStavku.Click += new System.EventHandler(this.btnIzmeniStavku_Click);
            // 
            // btnObrisiStavku
            // 
            this.btnObrisiStavku.Location = new System.Drawing.Point(256, 582);
            this.btnObrisiStavku.Name = "btnObrisiStavku";
            this.btnObrisiStavku.Size = new System.Drawing.Size(105, 35);
            this.btnObrisiStavku.TabIndex = 15;
            this.btnObrisiStavku.Text = "Delete item";
            this.btnObrisiStavku.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.btnObrisiStavku.UseSelectable = true;
            this.btnObrisiStavku.Click += new System.EventHandler(this.btnObrisiStavku_Click);
            // 
            // btnOK
            // 
            this.btnOK.Location = new System.Drawing.Point(318, 663);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(105, 36);
            this.btnOK.TabIndex = 17;
            this.btnOK.Text = "OK";
            this.btnOK.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.btnOK.UseSelectable = true;
            this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.Location = new System.Drawing.Point(429, 663);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(105, 36);
            this.btnCancel.TabIndex = 18;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.btnCancel.UseSelectable = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // txtBrLicKar
            // 
            // 
            // 
            // 
            this.txtBrLicKar.CustomButton.Image = null;
            this.txtBrLicKar.CustomButton.Location = new System.Drawing.Point(124, 1);
            this.txtBrLicKar.CustomButton.Name = "";
            this.txtBrLicKar.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.txtBrLicKar.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.txtBrLicKar.CustomButton.TabIndex = 1;
            this.txtBrLicKar.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.txtBrLicKar.CustomButton.UseSelectable = true;
            this.txtBrLicKar.CustomButton.Visible = false;
            this.txtBrLicKar.Lines = new string[0];
            this.txtBrLicKar.Location = new System.Drawing.Point(127, 257);
            this.txtBrLicKar.MaxLength = 32767;
            this.txtBrLicKar.Name = "txtBrLicKar";
            this.txtBrLicKar.PasswordChar = '\0';
            this.txtBrLicKar.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtBrLicKar.SelectedText = "";
            this.txtBrLicKar.SelectionLength = 0;
            this.txtBrLicKar.SelectionStart = 0;
            this.txtBrLicKar.ShortcutsEnabled = true;
            this.txtBrLicKar.Size = new System.Drawing.Size(146, 23);
            this.txtBrLicKar.TabIndex = 6;
            this.txtBrLicKar.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.txtBrLicKar.UseSelectable = true;
            this.txtBrLicKar.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.txtBrLicKar.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            this.txtBrLicKar.Click += new System.EventHandler(this.txtBrLicKar_Click);
            this.txtBrLicKar.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtBrLicKar_KeyPress);
            this.txtBrLicKar.Leave += new System.EventHandler(this.txtBrLicKar_Leave);
            // 
            // txtIme
            // 
            // 
            // 
            // 
            this.txtIme.CustomButton.Image = null;
            this.txtIme.CustomButton.Location = new System.Drawing.Point(124, 1);
            this.txtIme.CustomButton.Name = "";
            this.txtIme.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.txtIme.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.txtIme.CustomButton.TabIndex = 1;
            this.txtIme.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.txtIme.CustomButton.UseSelectable = true;
            this.txtIme.CustomButton.Visible = false;
            this.txtIme.Lines = new string[0];
            this.txtIme.Location = new System.Drawing.Point(127, 285);
            this.txtIme.MaxLength = 32767;
            this.txtIme.Name = "txtIme";
            this.txtIme.PasswordChar = '\0';
            this.txtIme.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtIme.SelectedText = "";
            this.txtIme.SelectionLength = 0;
            this.txtIme.SelectionStart = 0;
            this.txtIme.ShortcutsEnabled = true;
            this.txtIme.Size = new System.Drawing.Size(146, 23);
            this.txtIme.TabIndex = 7;
            this.txtIme.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.txtIme.UseSelectable = true;
            this.txtIme.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.txtIme.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            this.txtIme.Click += new System.EventHandler(this.txtIme_Click);
            this.txtIme.Leave += new System.EventHandler(this.txtIme_Leave);
            // 
            // txtPrezime
            // 
            // 
            // 
            // 
            this.txtPrezime.CustomButton.Image = null;
            this.txtPrezime.CustomButton.Location = new System.Drawing.Point(124, 1);
            this.txtPrezime.CustomButton.Name = "";
            this.txtPrezime.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.txtPrezime.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.txtPrezime.CustomButton.TabIndex = 1;
            this.txtPrezime.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.txtPrezime.CustomButton.UseSelectable = true;
            this.txtPrezime.CustomButton.Visible = false;
            this.txtPrezime.Lines = new string[0];
            this.txtPrezime.Location = new System.Drawing.Point(127, 310);
            this.txtPrezime.MaxLength = 32767;
            this.txtPrezime.Name = "txtPrezime";
            this.txtPrezime.PasswordChar = '\0';
            this.txtPrezime.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtPrezime.SelectedText = "";
            this.txtPrezime.SelectionLength = 0;
            this.txtPrezime.SelectionStart = 0;
            this.txtPrezime.ShortcutsEnabled = true;
            this.txtPrezime.Size = new System.Drawing.Size(146, 23);
            this.txtPrezime.TabIndex = 8;
            this.txtPrezime.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.txtPrezime.UseSelectable = true;
            this.txtPrezime.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.txtPrezime.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            this.txtPrezime.Click += new System.EventHandler(this.txtPrezime_Click);
            this.txtPrezime.Leave += new System.EventHandler(this.txtPrezime_Leave);
            // 
            // txtAdresa
            // 
            // 
            // 
            // 
            this.txtAdresa.CustomButton.Image = null;
            this.txtAdresa.CustomButton.Location = new System.Drawing.Point(124, 1);
            this.txtAdresa.CustomButton.Name = "";
            this.txtAdresa.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.txtAdresa.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.txtAdresa.CustomButton.TabIndex = 1;
            this.txtAdresa.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.txtAdresa.CustomButton.UseSelectable = true;
            this.txtAdresa.CustomButton.Visible = false;
            this.txtAdresa.Lines = new string[0];
            this.txtAdresa.Location = new System.Drawing.Point(357, 257);
            this.txtAdresa.MaxLength = 32767;
            this.txtAdresa.Name = "txtAdresa";
            this.txtAdresa.PasswordChar = '\0';
            this.txtAdresa.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtAdresa.SelectedText = "";
            this.txtAdresa.SelectionLength = 0;
            this.txtAdresa.SelectionStart = 0;
            this.txtAdresa.ShortcutsEnabled = true;
            this.txtAdresa.Size = new System.Drawing.Size(146, 23);
            this.txtAdresa.TabIndex = 9;
            this.txtAdresa.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.txtAdresa.UseSelectable = true;
            this.txtAdresa.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.txtAdresa.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            this.txtAdresa.Click += new System.EventHandler(this.txtAdresa_Click);
            // 
            // txtBrTel
            // 
            // 
            // 
            // 
            this.txtBrTel.CustomButton.Image = null;
            this.txtBrTel.CustomButton.Location = new System.Drawing.Point(124, 1);
            this.txtBrTel.CustomButton.Name = "";
            this.txtBrTel.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.txtBrTel.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.txtBrTel.CustomButton.TabIndex = 1;
            this.txtBrTel.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.txtBrTel.CustomButton.UseSelectable = true;
            this.txtBrTel.CustomButton.Visible = false;
            this.txtBrTel.Lines = new string[0];
            this.txtBrTel.Location = new System.Drawing.Point(357, 285);
            this.txtBrTel.MaxLength = 32767;
            this.txtBrTel.Name = "txtBrTel";
            this.txtBrTel.PasswordChar = '\0';
            this.txtBrTel.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtBrTel.SelectedText = "";
            this.txtBrTel.SelectionLength = 0;
            this.txtBrTel.SelectionStart = 0;
            this.txtBrTel.ShortcutsEnabled = true;
            this.txtBrTel.Size = new System.Drawing.Size(146, 23);
            this.txtBrTel.TabIndex = 10;
            this.txtBrTel.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.txtBrTel.UseSelectable = true;
            this.txtBrTel.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.txtBrTel.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            this.txtBrTel.Click += new System.EventHandler(this.txtBrTel_Click);
            this.txtBrTel.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtBrTel_KeyPress);
            // 
            // txtEmail
            // 
            // 
            // 
            // 
            this.txtEmail.CustomButton.Image = null;
            this.txtEmail.CustomButton.Location = new System.Drawing.Point(124, 1);
            this.txtEmail.CustomButton.Name = "";
            this.txtEmail.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.txtEmail.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.txtEmail.CustomButton.TabIndex = 1;
            this.txtEmail.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.txtEmail.CustomButton.UseSelectable = true;
            this.txtEmail.CustomButton.Visible = false;
            this.txtEmail.Lines = new string[0];
            this.txtEmail.Location = new System.Drawing.Point(357, 310);
            this.txtEmail.MaxLength = 32767;
            this.txtEmail.Name = "txtEmail";
            this.txtEmail.PasswordChar = '\0';
            this.txtEmail.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtEmail.SelectedText = "";
            this.txtEmail.SelectionLength = 0;
            this.txtEmail.SelectionStart = 0;
            this.txtEmail.ShortcutsEnabled = true;
            this.txtEmail.Size = new System.Drawing.Size(146, 23);
            this.txtEmail.TabIndex = 11;
            this.txtEmail.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.txtEmail.UseSelectable = true;
            this.txtEmail.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.txtEmail.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            this.txtEmail.Click += new System.EventHandler(this.txtEmail_Click);
            // 
            // cmbStatus
            // 
            this.cmbStatus.FormattingEnabled = true;
            this.cmbStatus.ItemHeight = 23;
            this.cmbStatus.Items.AddRange(new object[] {
            "Pending",
            "Processing",
            "Complete"});
            this.cmbStatus.Location = new System.Drawing.Point(80, 636);
            this.cmbStatus.Name = "cmbStatus";
            this.cmbStatus.Size = new System.Drawing.Size(129, 29);
            this.cmbStatus.TabIndex = 16;
            this.cmbStatus.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.cmbStatus.UseSelectable = true;
            // 
            // dgvOrderList
            // 
            this.dgvOrderList.AllowUserToResizeRows = false;
            this.dgvOrderList.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            this.dgvOrderList.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dgvOrderList.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.dgvOrderList.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(174)))), ((int)(((byte)(219)))));
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            dataGridViewCellStyle1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(198)))), ((int)(((byte)(247)))));
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvOrderList.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dgvOrderList.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            dataGridViewCellStyle2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(198)))), ((int)(((byte)(247)))));
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvOrderList.DefaultCellStyle = dataGridViewCellStyle2;
            this.dgvOrderList.EnableHeadersVisualStyles = false;
            this.dgvOrderList.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.dgvOrderList.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            this.dgvOrderList.Location = new System.Drawing.Point(27, 375);
            this.dgvOrderList.Name = "dgvOrderList";
            this.dgvOrderList.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(174)))), ((int)(((byte)(219)))));
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            dataGridViewCellStyle3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(198)))), ((int)(((byte)(247)))));
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvOrderList.RowHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.dgvOrderList.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.dgvOrderList.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvOrderList.Size = new System.Drawing.Size(506, 188);
            this.dgvOrderList.TabIndex = 12;
            this.dgvOrderList.Theme = MetroFramework.MetroThemeStyle.Dark;
            // 
            // FormNova
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(551, 710);
            this.Controls.Add(this.dgvOrderList);
            this.Controls.Add(this.cmbStatus);
            this.Controls.Add(this.txtEmail);
            this.Controls.Add(this.txtBrTel);
            this.Controls.Add(this.txtAdresa);
            this.Controls.Add(this.txtPrezime);
            this.Controls.Add(this.txtIme);
            this.Controls.Add(this.txtBrLicKar);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnOK);
            this.Controls.Add(this.btnObrisiStavku);
            this.Controls.Add(this.btnIzmeniStavku);
            this.Controls.Add(this.btnDodajStavku);
            this.Controls.Add(this.lblStatus);
            this.Controls.Add(this.lblOrderList);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.lblCustomer);
            this.Controls.Add(this.txtFreightCharges);
            this.Controls.Add(this.txtShipVia);
            this.Controls.Add(this.dtpDateShipped);
            this.Controls.Add(this.dtpDateRequired);
            this.Controls.Add(this.lblFreightCharges);
            this.Controls.Add(this.lblShipVia);
            this.Controls.Add(this.lblDateShipped);
            this.Controls.Add(this.lblDateRequired);
            this.Controls.Add(this.dtpOrderDate);
            this.Controls.Add(this.txtOrderID);
            this.Controls.Add(this.lblOrderDate);
            this.Controls.Add(this.lblOrderID);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FormNova";
            this.Text = "Order";
            this.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FormNova_FormClosing);
            this.Load += new System.EventHandler(this.FormNova_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgvOrderList)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private MetroFramework.Controls.MetroTile lblOrderID;
        private MetroFramework.Controls.MetroTile lblOrderDate;
        private MetroFramework.Controls.MetroTextBox txtOrderID;
        private MetroFramework.Controls.MetroDateTime dtpOrderDate;
        private MetroFramework.Controls.MetroTile lblDateRequired;
        private MetroFramework.Controls.MetroTile lblDateShipped;
        private MetroFramework.Controls.MetroTile lblShipVia;
        private MetroFramework.Controls.MetroTile lblFreightCharges;
        private MetroFramework.Controls.MetroDateTime dtpDateRequired;
        private MetroFramework.Controls.MetroDateTime dtpDateShipped;
        private MetroFramework.Controls.MetroTextBox txtShipVia;
        private MetroFramework.Controls.MetroTextBox txtFreightCharges;
        private MetroFramework.Controls.MetroTile lblCustomer;
        private MetroFramework.Controls.MetroLabel label1;
        private MetroFramework.Controls.MetroLabel label2;
        private MetroFramework.Controls.MetroLabel label3;
        private MetroFramework.Controls.MetroLabel label4;
        private MetroFramework.Controls.MetroLabel label5;
        private MetroFramework.Controls.MetroLabel label6;
        private MetroFramework.Controls.MetroLabel lblOrderList;
        private MetroFramework.Controls.MetroLabel lblStatus;
        private MetroFramework.Controls.MetroButton btnDodajStavku;
        private MetroFramework.Controls.MetroButton btnIzmeniStavku;
        private MetroFramework.Controls.MetroButton btnObrisiStavku;
        private MetroFramework.Controls.MetroButton btnOK;
        private MetroFramework.Controls.MetroButton btnCancel;
        private MetroFramework.Controls.MetroTextBox txtBrLicKar;
        private MetroFramework.Controls.MetroTextBox txtIme;
        private MetroFramework.Controls.MetroTextBox txtPrezime;
        private MetroFramework.Controls.MetroTextBox txtAdresa;
        private MetroFramework.Controls.MetroTextBox txtBrTel;
        private MetroFramework.Controls.MetroTextBox txtEmail;
        private MetroFramework.Controls.MetroComboBox cmbStatus;
        private MetroFramework.Controls.MetroGrid dgvOrderList;
    }
}