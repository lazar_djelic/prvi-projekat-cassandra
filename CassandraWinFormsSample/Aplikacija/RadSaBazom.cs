﻿using Podaci;
using Cassandra;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aplikacija
{
    class RadSaBazom
    {
        #region Narudzbenica
        public static List<Narudzbenica> ucitajNarudzbeniceIzBaze()
        {
            List<Narudzbenica> ListaNarudzbenica = new List<Narudzbenica>();

            ISession session = SessionManager.GetSession();
            if (session == null)
                return null;

            var narudzbeniceData = session.Execute("select * from narudzbenica");

            foreach (var narudzbenicaData in narudzbeniceData)
            {
                Narudzbenica narudzbenica = new Narudzbenica();
                String tmp = narudzbenicaData["orderID"].ToString();
                narudzbenica.Order_ID = long.Parse(tmp) != null ? long.Parse(tmp) : 0;
                narudzbenica.Purchased_On = narudzbenicaData["purchasedon"] != null ? narudzbenicaData["purchasedon"].ToString() : string.Empty;
                String tmp1 = narudzbenicaData["income"].ToString();
                narudzbenica.Income = Decimal.Parse(tmp1) != null ? Decimal.Parse(tmp1) : 0;
                narudzbenica.Status = narudzbenicaData["status"] != null ? narudzbenicaData["status"].ToString() : string.Empty;
                narudzbenica.Date_Required = narudzbenicaData["daterequired"] != null ? narudzbenicaData["daterequired"].ToString() : string.Empty;
                narudzbenica.Date_Shipped = narudzbenicaData["dateshipped"] != null ? narudzbenicaData["dateshipped"].ToString() : string.Empty;
                narudzbenica.Ship_Via = narudzbenicaData["shipvia"] != null ? narudzbenicaData["shipvia"].ToString() : string.Empty;
                String tmp2 = narudzbenicaData["freightcharges"].ToString();
                narudzbenica.Freight_Charges = Decimal.Parse(tmp2) != null ? Decimal.Parse(tmp2) : 0;
                String tmp3 = narudzbenicaData["narucilacLK"].ToString();
                narudzbenica.Narucilac = Int32.Parse(tmp3) != null ? Int32.Parse(tmp3) : 0;
                ListaNarudzbenica.Add(narudzbenica);
            }

            return ListaNarudzbenica;
        }

        public static Narudzbenica vratiNarudzbenicu(Int64 id)
        {
            Narudzbenica narudzbenica = new Narudzbenica();

            ISession session = SessionManager.GetSession();
            if (session == null)
                return null;

            Row narudzbenicaData = session.Execute("select * from narudzbenica where \"orderID\"=" + id).FirstOrDefault();

            if (narudzbenicaData != null)
            {
                String tmp = narudzbenicaData["orderID"].ToString();
                narudzbenica.Order_ID = long.Parse(tmp) != null ? long.Parse(tmp) : 0;
                narudzbenica.Purchased_On = narudzbenicaData["purchasedon"] != null ? narudzbenicaData["purchasedon"].ToString() : string.Empty;
                String tmp1 = narudzbenicaData["income"].ToString();
                narudzbenica.Income = Decimal.Parse(tmp1) != null ? Decimal.Parse(tmp1) : 0;
                narudzbenica.Status = narudzbenicaData["status"] != null ? narudzbenicaData["status"].ToString() : string.Empty;
                narudzbenica.Date_Required = narudzbenicaData["daterequired"] != null ? narudzbenicaData["daterequired"].ToString() : string.Empty;
                narudzbenica.Date_Shipped = narudzbenicaData["dateshipped"] != null ? narudzbenicaData["dateshipped"].ToString() : string.Empty;
                narudzbenica.Ship_Via = narudzbenicaData["shipvia"] != null ? narudzbenicaData["shipvia"].ToString() : string.Empty;
                String tmp2 = narudzbenicaData["freightcharges"].ToString();
                narudzbenica.Freight_Charges = Decimal.Parse(tmp2) != null ? Decimal.Parse(tmp2) : 0;
                String tmp3 = narudzbenicaData["narucilacLK"].ToString();
                narudzbenica.Narucilac = Int32.Parse(tmp3) != null ? Int32.Parse(tmp3) : 0;
            }

            return narudzbenica;
        }

        public static void obrisiNarudzbenicu(Int64 id)
        {
            ISession session = SessionManager.GetSession();
            Narudzbenica narudzbenica = new Narudzbenica();

            if (session == null)
                return;

            RowSet narudzbenicaData = session.Execute("delete from narudzbenica where \"orderID\" = " + id);
        }

        public static void promeniStatusNarudzbenici(String statusN, Int64 id)
        {
            ISession session = SessionManager.GetSession();
            if (session == null)
                return;

            RowSet narudzbenicaData = session.Execute("UPDATE narudzbenica SET status='" + statusN + "' where \"orderID\" = " + id);
        }

        public static void dodajNarudzbenicu(Int64 orderID, string purchasedOn, Decimal income, string status, string dateRequired, string dateShipped, string shipVia, Decimal freightCharges, string narucilacLK)
        {
            ISession session = SessionManager.GetSession();
            if (session == null)
                return;

            RowSet orderData = session.Execute("insert into narudzbenica (\"orderID\", purchasedOn, income, status, dateRequired, dateShipped, shipVia, freightCharges, \"narucilacLK\")  values (" + orderID + ", '" + purchasedOn + "',  " + income + ", '" + status + "', '" + dateRequired + "', '" + dateShipped + "', '" + shipVia + "', " + freightCharges + ", " + narucilacLK + ")");
        }

        public static void izmeniNarudzbenicu(Int64 orderID, string purchasedOn, Decimal income, string status, string dateRequired, string dateShipped, string shipVia, Decimal freightCharges)
        {
            ISession session = SessionManager.GetSession();

            if (session == null)
                return;

            RowSet orderData = session.Execute("UPDATE narudzbenica SET daterequired = '" + dateRequired + "', dateshipped = '" + dateShipped + "', freightcharges = " + freightCharges + ", income = " + income + ", purchasedon = '" + purchasedOn + "', shipvia = '" + shipVia + "', status = '" + status + "' WHERE \"orderID\" = " + orderID + ";");
        }
        #endregion

        #region Narucilac
        public static void dodajNarucioca(Int32 brLicneKarte, string ime, string prezime, string adresa, string brTelefona, string email)
        {
            ISession session = SessionManager.GetSession();
            if (session == null)
                return;

            Narucilac n = ucitajNarucioca(brLicneKarte);

            if (n == null)
                return;

            RowSet narucilacData = session.Execute("insert into narucilac (\"brLicneKarte\", ime, prezime, adresa, brTelefona, email)  values (" + brLicneKarte + ", '" + ime + "',  '" + prezime + "', '" + adresa + "', '" + brTelefona + "', '" + email + "')");
        }

        public static void izmeniNarucioca(string brLicneKarte, string ime, string prezime, string adresa, string brTelefona, string email)
        {
            ISession session = SessionManager.GetSession();

            if (session == null)
                return;

            RowSet narucilacData = session.Execute("UPDATE narucilac SET ime = '" + ime + "', prezime = '" + prezime + "', adresa = '" + adresa + "', brTelefona = '" + brTelefona + "', email = '" + email + "' WHERE \"brLicneKarte\" = " + brLicneKarte + ";");
        }

        public static Narucilac ucitajNarucioca(Int32 brLicneKarte)
        {
            Narucilac narucilac = new Narucilac();

            ISession session = SessionManager.GetSession();
            if (session == null)
                return null;

            Row narucilacData = session.Execute("select * from narucilac where \"brLicneKarte\"=" + brLicneKarte).FirstOrDefault();

            if (narucilacData != null)
            {
                string tmp = narucilacData["brLicneKarte"].ToString();
                narucilac.Licna_Karta = Int32.Parse(tmp) != null ? Int32.Parse(tmp) : 0;
                narucilac.Ime = narucilacData["ime"] != null ? narucilacData["ime"].ToString() : string.Empty;
                narucilac.Prezime = narucilacData["prezime"] != null ? narucilacData["prezime"].ToString() : string.Empty;
                narucilac.Adresa = narucilacData["adresa"] != null ? narucilacData["adresa"].ToString() : string.Empty;
                narucilac.Broj_Telefona = narucilacData["brtelefona"] != null ? narucilacData["brtelefona"].ToString() : string.Empty;
                narucilac.Email = narucilacData["email"] != null ? narucilacData["email"].ToString() : string.Empty;
            }

            return narucilac;
        }
        #endregion

        #region Stavka
        public static Stavka ucitajStavkuIzBaze(Int64 stavkaID)
        {
            Stavka stavka = new Stavka();

            ISession session = SessionManager.GetSession();
            if (session == null)
                return null;

            Row stavkaData = session.Execute("select * from stavka where \"stavkaID\"=" + stavkaID).FirstOrDefault();

            if (stavkaData != null)
            {
                String tmp = stavkaData["stavkaID"].ToString();
                stavka.Item_ID = Int64.Parse(tmp) != null ? Int64.Parse(tmp) : 0;
                stavka.Product_Name = stavkaData["naziv"] != null ? stavkaData["naziv"].ToString() : string.Empty;
                String tmp2 = stavkaData["cena"].ToString();
                stavka.Unit_Price = Decimal.Parse(tmp2) != null ? Decimal.Parse(tmp2) : 0;
                String tmp3 = stavkaData["kolicina"].ToString();
                stavka.Quantity = Int32.Parse(tmp3) != null ? Int32.Parse(tmp3) : 0;
                String tmp4 = stavkaData["narudzbenicaID"].ToString();
                stavka.OrderID = Int64.Parse(tmp4) != null ? Int64.Parse(tmp4) : 0;
            }

            return stavka;
        }

        public static List<Stavka> ucitajStavkeIzBaze(Int64 narudzbenicaID)
        {
            List<Stavka> ListaStavki = new List<Stavka>();

            ISession session = SessionManager.GetSession();
            if (session == null)
                return null;

            var stavkeData = session.Execute("select * from stavka where \"narudzbenicaID\" = " + narudzbenicaID + " ALLOW FILTERING");

            foreach (var stavkaData in stavkeData)
            {
                Stavka stavka = new Stavka();
                String tmp = stavkaData["stavkaID"].ToString();
                stavka.Item_ID = Int64.Parse(tmp) != null ? Int64.Parse(tmp) : 0;
                stavka.Product_Name = stavkaData["naziv"] != null ? stavkaData["naziv"].ToString() : string.Empty;
                String tmp1 = stavkaData["cena"].ToString();
                stavka.Unit_Price = Decimal.Parse(tmp1) != null ? Decimal.Parse(tmp1) : 0;
                String tmp2 = stavkaData["kolicina"].ToString();
                stavka.Quantity = Int32.Parse(tmp2) != null ? Int32.Parse(tmp2) : 0;
                ListaStavki.Add(stavka);
            }

            return ListaStavki;
        }

        public static void obrisiStavku(Int64 stavkaID)
        {
            ISession session = SessionManager.GetSession();
            if (session == null)
                return;

            RowSet stavkaData = session.Execute("delete from stavka where \"stavkaID\" = " + stavkaID);
        }

        public static Int32 nadjiIDZaStavku()
        {
            ISession session = SessionManager.GetSession();
            if (session == null)
                return 0;

            var stavkeData = session.Execute("select * from stavka");

            Int32 idZaStavku = stavkeData.Count();

            return idZaStavku;
        }

        public static void dodajStavku(Int64 stavkaID, string naziv, Decimal cena, Int32 kolicina, Int64 narudzbenicaID)
        {
            ISession session = SessionManager.GetSession();

            if (session == null)
                return;

            RowSet hotelData = session.Execute("insert into stavka (\"stavkaID\", naziv, cena, kolicina, \"narudzbenicaID\")  values (" + stavkaID + ", '" + naziv + "', " + cena + ", " + kolicina + ", " + narudzbenicaID + ")");
        }

        public static void izmeniStavku(Int64 stavkaID, string naziv, Decimal cena, Int32 kolicina, Int64 narudzbenicaID)
        {
            ISession session = SessionManager.GetSession();

            if (session == null)
                return;

            RowSet itemData = session.Execute("UPDATE stavka SET naziv = '" + naziv + "', cena = " + cena + ", kolicina = " + kolicina + ", \"narudzbenicaID\" = " + narudzbenicaID + " WHERE \"stavkaID\" = " + stavkaID + ";");
        }
        #endregion
    }
}
