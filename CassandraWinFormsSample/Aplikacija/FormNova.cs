﻿using Podaci;
using Ekstenzije;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Aplikacija
{
    public partial class FormNova : MetroFramework.Forms.MetroForm
    {
        private bool _formaZatvorena, tacka = false;
        private List<Stavka> ListaStavki;
        private Int64 orderIDzaIzmenu;
        private Int32 idZaStavku = 0;
        private Narucilac n;
        public Narudzbenica Order
        {
            get;
            set;
        }

        public FormNova(Narudzbenica n)
        {
            ListaStavki = new List<Stavka>();
            Order = n;
            InitializeComponent();
        }

        DialogResult ZatvoriFormu()
        {
            return MessageBox.Show("Da li ste sigurni da izvrsite izabranu akciju?", "Obavestenje", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
        }

        List<Stavka> SortList(List<Stavka> lista)
        {
            for (int i = 0; i < lista.Count - 1; i++)
            {
                String naziv = lista[i].Product_Name;
                for (int j = i + 1; j < lista.Count; j++)
                {
                    String naziv2 = lista[j].Product_Name;
                    if (String.Compare(naziv, naziv2) > -1)
                    {
                        Stavka tmp = lista[i];
                        lista[i] = lista[j];
                        lista[j] = tmp;
                    }
                }
            }
            return lista;
        }

        bool Provera()
        {
            bool indicator = false;
            if (String.IsNullOrEmpty(txtOrderID.Text))
            {
                txtOrderID.BackColor = System.Drawing.Color.Red;
                indicator = true;
            }

            if (String.IsNullOrEmpty(txtShipVia.Text))
            {
                txtShipVia.BackColor = System.Drawing.Color.Red;
                indicator = true;
            }

            if (String.IsNullOrEmpty(txtBrLicKar.Text))
            {
                txtBrLicKar.BackColor = System.Drawing.Color.Red;
                indicator = true;
            }

            if (String.IsNullOrEmpty(txtIme.Text))
            {
                txtIme.BackColor = System.Drawing.Color.Red;
                indicator = true;
            }

            if (String.IsNullOrEmpty(txtPrezime.Text))
            {
                txtPrezime.BackColor = System.Drawing.Color.Red;
                indicator = true;
            }

            if (String.IsNullOrEmpty(txtAdresa.Text))
            {
                txtAdresa.BackColor = System.Drawing.Color.Red;
                indicator = true;
            }

            if (String.IsNullOrEmpty(txtBrTel.Text))
            {
                txtBrTel.BackColor = System.Drawing.Color.Red;
                indicator = true;
            }

            if (String.IsNullOrEmpty(txtEmail.Text))
            {
                txtEmail.BackColor = System.Drawing.Color.Red;
                indicator = true;
            }

            if (txtOrderID.Text.Length < 8)
            {
                txtOrderID.BackColor = System.Drawing.Color.Red;
                MessageBox.Show("Order ID mora sadrzati 8 cifara.", "Obavestenje", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return false;
            }

            if (indicator)
            {
                MessageBox.Show("Morate uneti vrednosti obaveznih polja!", "Obavestenje", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }

            return true;
        }

        void Dodaj()
        {
            Int32 id;
            Int32.TryParse(txtOrderID.Text, out id);
            Int32 narcilacid;
            Int32.TryParse(txtBrLicKar.Text, out narcilacid);
            Decimal fc;
            Decimal.TryParse(txtFreightCharges.Text, out fc);
            Decimal income = 0;

            foreach (var s in ListaStavki)
                income = income + (s.Unit_Price * s.Quantity);

            RadSaBazom.dodajNarudzbenicu(id, dtpOrderDate.Text, income, cmbStatus.Text, dtpDateRequired.Text, dtpDateShipped.Text, txtShipVia.Text, fc, txtBrLicKar.Text);

            RadSaBazom.dodajNarucioca(narcilacid, txtIme.Text, txtPrezime.Text, txtAdresa.Text, txtBrTel.Text, txtEmail.Text);

            return;
        }

        void Izmeni()
        {
            Int32 id;
            Int32.TryParse(txtOrderID.Text, out id);
            Decimal fc;
            Decimal.TryParse(txtFreightCharges.Text, out fc);
            Decimal income = 0;

            foreach (var s in ListaStavki)
                income = income + (s.Unit_Price * s.Quantity);

            RadSaBazom.izmeniNarudzbenicu(orderIDzaIzmenu, dtpOrderDate.Text, income, cmbStatus.Text, dtpDateRequired.Text, dtpDateShipped.Text, txtShipVia.Text, fc);

            RadSaBazom.izmeniNarucioca(txtBrLicKar.Text, txtIme.Text, txtPrezime.Text, txtAdresa.Text, txtBrTel.Text, txtEmail.Text);

            return;
        }

        void ucitajNarucioca()
        {
            Narucilac narucilac = RadSaBazom.ucitajNarucioca(Order.Narucilac);

            txtBrLicKar.Text = narucilac.Licna_Karta.ToString();
            txtIme.Text = narucilac.Ime;
            txtPrezime.Text = narucilac.Prezime;
            txtAdresa.Text = narucilac.Adresa;
            txtBrTel.Text = narucilac.Broj_Telefona;
            txtEmail.Text = narucilac.Email;

            return;
        }

        void UcitajPodatkeUPolja()
        {
            if (Order.Order_ID != 0)
            {
                txtOrderID.Text = Order.Order_ID.ToString();
                dtpOrderDate.Value = System.DateTime.Parse(Order.Purchased_On);
                dtpDateRequired.Value = System.DateTime.Parse(Order.Date_Required);
                txtShipVia.Text = Order.Ship_Via;
                txtFreightCharges.Text = Order.Freight_Charges.ToString();
                // txtFreightCharges.Text = Order.Freight_Charges.ToString("0.00");
                ucitajNarucioca();
                UcitajStavkeUListu();
                cmbStatus.Text = Order.Status;
                if (Order.Date_Shipped != " ")
                    dtpDateShipped.Value = System.DateTime.Parse(Order.Date_Shipped);
                else
                {
                    dtpDateShipped.CustomFormat = " ";
                    dtpDateShipped.Format = DateTimePickerFormat.Custom;
                }
            }
            else
            {
                dtpDateShipped.CustomFormat = " ";
                dtpDateShipped.Format = DateTimePickerFormat.Custom;
            }

        }

        public void ucitajIzBaze()
        {
            ListaStavki = RadSaBazom.ucitajStavkeIzBaze(Order.Order_ID);

            idZaStavku = RadSaBazom.nadjiIDZaStavku();

            return;
        }

        void UcitajStavkeUListu()
        {
            ucitajIzBaze();

            List<Stavka> lista = SortList(ListaStavki);
            var source = new BindingSource();
            source.DataSource = lista;
            dgvOrderList.DataSource = source;

            if (dgvOrderList.RowCount > 0)
            {
                btnObrisiStavku.Enabled = true;
                btnIzmeniStavku.Enabled = true;
            }
            else
            {
                btnObrisiStavku.Enabled = false;
                btnIzmeniStavku.Enabled = false;
            }
            dgvOrderList.Columns[0].HeaderText = "Item ID";
            dgvOrderList.Columns[1].Visible = false;
            dgvOrderList.Columns[2].HeaderText = "Product Name";
            dgvOrderList.Columns[3].HeaderText = "Unit Price";
        }

        private void FormNova_Load(object sender, EventArgs e)
        {
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            n = new Narucilac();
            if (Order.Order_ID != 0)
            {
                ucitajNarucioca();
                UcitajPodatkeUPolja();
                UcitajStavkeUListu();
                orderIDzaIzmenu = Order.Order_ID;
                txtBrLicKar.Enabled = false;
            }
            else
            {
                btnObrisiStavku.Enabled = false;
                btnIzmeniStavku.Enabled = false;
                dtpDateShipped.CustomFormat = " ";
                dtpDateShipped.Format = DateTimePickerFormat.Custom;
            }
        }

        private void FormNova_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (_formaZatvorena)
                return;

            if (ZatvoriFormu() == DialogResult.Yes)
                e.Cancel = false;
            else
                e.Cancel = true;
        }

        private void btnDodajStavku_Click(object sender, EventArgs e)
        {
            var forma = new FormStavka(Order, idZaStavku);
            DialogResult dr = forma.ShowDialog();

            if (dr == System.Windows.Forms.DialogResult.OK)
                UcitajStavkeUListu();
        }

        private void btnIzmeniStavku_Click(object sender, EventArgs e)
        {
            if (dgvOrderList.SelectedRows.Count == 0)
                return;
            
            int izabranaStavka = dgvOrderList.SelectedRows[0].Index;
            Int64 id = (Int64)dgvOrderList["Item_ID", izabranaStavka].Value;

            Stavka stavka = RadSaBazom.ucitajStavkuIzBaze(id);

            var frm = new FormStavka(stavka, Order);

            DialogResult dlg = frm.ShowDialog();

            if (dlg == System.Windows.Forms.DialogResult.OK)
                UcitajStavkeUListu();
        }

        private void btnObrisiStavku_Click(object sender, EventArgs e)
        {
            if (dgvOrderList.SelectedRows.Count == 0)
                return;

            DialogResult dlg = MessageBox.Show("Da li ste sigurni da zelite da obrisete izabranu stavku?", "Obavestenje", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

            if (dlg == System.Windows.Forms.DialogResult.No)
                return;

            int izabranaStavka = dgvOrderList.SelectedRows[0].Index;
            Int64 id = (Int64)dgvOrderList["Item_ID", izabranaStavka].Value;

            RadSaBazom.obrisiStavku(id);

            UcitajStavkeUListu();
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            if (!Provera())
                return;

            if (Order.Order_ID == 0)
                Dodaj();
            else
                Izmeni();

            this.Close();
            DialogResult = System.Windows.Forms.DialogResult.OK;
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
            DialogResult = System.Windows.Forms.DialogResult.Cancel;
        }

        private void txtOrderID_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!Char.IsDigit(e.KeyChar) && !Char.IsControl(e.KeyChar))
                e.Handled = true;
        }

        private void txtOrderID_Click(object sender, EventArgs e)
        {
            txtOrderID.BackColor = System.Drawing.Color.White;
        }

        private void txtShipVia_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!Char.IsLetter(e.KeyChar) && !Char.IsControl(e.KeyChar) && e.KeyChar.ToString() != " ")
                e.Handled = true;
        }

        private void txtShipVia_Click(object sender, EventArgs e)
        {
            txtShipVia.BackColor = System.Drawing.Color.White;
        }

        private void txtShipVia_Leave(object sender, EventArgs e)
        {
            txtShipVia.Text = txtShipVia.Text.PostaviPrvoVelikoSlovo();
        }

        private void txtFreightCharges_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!tacka)
            {
                if (!Char.IsDigit(e.KeyChar) && !Char.IsControl(e.KeyChar) && e.KeyChar.ToString() != ".")
                    e.Handled = true;
            }
            else
                if (!Char.IsDigit(e.KeyChar) && !Char.IsControl(e.KeyChar))
                    e.Handled = true;

            if (e.KeyChar.ToString() == ".") { tacka = true; }
        }

        private void dtpDateShipped_ValueChanged(object sender, EventArgs e)
        {
            dtpDateShipped.CustomFormat = "dd/MM/yyyy";
        }

        private void txtBrLicKar_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!Char.IsDigit(e.KeyChar) && !Char.IsControl(e.KeyChar))
                e.Handled = true;
        }

        private void txtBrTel_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!Char.IsDigit(e.KeyChar) && !Char.IsControl(e.KeyChar))
                e.Handled = true;
        }

        private void txtIme_Leave(object sender, EventArgs e)
        {
            txtIme.Text = txtIme.Text.PostaviPrvoVelikoSlovo();
        }

        private void txtPrezime_Leave(object sender, EventArgs e)
        {
            txtPrezime.Text = txtPrezime.Text.PostaviPrvoVelikoSlovo();
        }

        private void txtBrLicKar_Click(object sender, EventArgs e)
        {
            txtBrLicKar.BackColor = System.Drawing.Color.White;
        }

        private void txtIme_Click(object sender, EventArgs e)
        {
            txtIme.BackColor = System.Drawing.Color.White;
        }

        private void txtPrezime_Click(object sender, EventArgs e)
        {
            txtPrezime.BackColor = System.Drawing.Color.White;
        }

        private void txtAdresa_Click(object sender, EventArgs e)
        {
            txtAdresa.BackColor = System.Drawing.Color.White;
        }

        private void txtBrTel_Click(object sender, EventArgs e)
        {
            txtBrTel.BackColor = System.Drawing.Color.White;
        }

        private void metroTile1_Click(object sender, EventArgs e)
        {

        }

        private void lblOrderList_Click(object sender, EventArgs e)
        {

        }

        private void txtEmail_Click(object sender, EventArgs e)
        {
            txtEmail.BackColor = System.Drawing.Color.White;
        }

        private void txtBrLicKar_Leave(object sender, EventArgs e)
        {
            if (txtBrLicKar.Text != "")
            {
                Int32 lk = Int32.Parse(txtBrLicKar.Text);
                if (lk != 0)
                    n = RadSaBazom.ucitajNarucioca(lk);

                if (n != null)
                {
                    txtIme.Text = n.Ime;
                    txtPrezime.Text = n.Prezime;
                    txtAdresa.Text = n.Adresa;
                    txtBrTel.Text = n.Broj_Telefona;
                    txtEmail.Text = n.Email;
                }
            }
        }
    }
}
